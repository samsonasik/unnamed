<?php

/**
 * @copyright  2015 (c) Stanimir Dimitrov.
 * @license    http://www.opensource.org/licenses/mit-license.php  MIT License
 *
 * @version    0.0.14
 *
 * @link       TBA
 */

namespace Application\Controller\Plugin\Factory;

use Application\Controller\Plugin\Mailing;
use Zend\Mvc\Controller\PluginManager;

class MailingFactory
{
    /**
     * @{inheritDoc}
     */
    public function __invoke(PluginManager $pluginManager)
    {
        $flashmessenger = $pluginManager->get("flashmessenger");

        $plugin = new Mailing($flashmessenger);

        return $plugin;
    }
}
